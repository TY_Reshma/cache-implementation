package com.te.cache.service;

import java.util.List;

import com.te.cache.entity.Employee;

public interface EmployeeService {

	Employee addEmployee(Employee employee);
	
	List<Employee> getAllEmployees();
	
	Employee updatEmployee(Employee employee, Integer id);
	
	Employee findById(Integer id);
	
	List<Employee> findByName(String name);
	
	Employee deleteEmployee(Integer id);
}
