package com.te.cache.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.te.cache.entity.Employee;
import com.te.cache.repository.EmployeeRepository;
@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository dao;

	@Override
	public Employee addEmployee(Employee employee) {
		return dao.save(employee);
	}

	@Override
	public List<Employee> getAllEmployees() {
		return dao.findAll();
	}

	@Override
	@CachePut(cacheNames = "Employee", key = "#id")
	public Employee updatEmployee(Employee employee,Integer id) {
		System.out.println("before cache update");
		Employee employee2 = dao.findById(id).get();
		employee2.setId(employee.getId());
		return dao.save(employee);
	}

	@Cacheable(cacheNames = "findById", key = "#id")
	@Override
	public Employee findById(Integer id) {
		System.out.println("Before Caching");
		return dao.findById(id).get();
	}	
	@Override
	public List<Employee> findByName(String name) {
		return dao.findByName(name);
	}

	@CacheEvict(cacheNames = "deleteEmployee", key = "#id")
	@Override
	public Employee deleteEmployee(Integer id) {
		dao.deleteById(id);
		return null;
	}
}
