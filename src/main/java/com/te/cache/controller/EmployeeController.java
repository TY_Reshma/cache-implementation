package com.te.cache.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.te.cache.entity.Employee;
import com.te.cache.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService service;

	@PostMapping("/add")
	ResponseEntity<?> addEmployee(@RequestBody Employee employee) {
		Employee addEmployee = service.addEmployee(employee);
		return new ResponseEntity<Employee>(addEmployee, HttpStatus.OK);
	}

	@GetMapping("/all")
	ResponseEntity<?> getAllEmployees() {
		List<Employee> allEmployees = service.getAllEmployees();
		return new ResponseEntity<List<Employee>>(allEmployees, HttpStatus.OK);
	}

	@PutMapping("/update/{id}") // http://localhost:8080/update/3
	ResponseEntity<?> update(@RequestBody Employee employee, @PathVariable Integer id) {
		Employee updatEmployee = service.updatEmployee(employee, id);
		return new ResponseEntity<Employee>(updatEmployee, HttpStatus.OK);
	}

	@GetMapping("/find/{id}") // http://localhost:8080/find/3
	ResponseEntity<?> findById(@PathVariable Integer id) {
		Employee findById = service.findById(id);
		return new ResponseEntity<Employee>(findById, HttpStatus.OK);
	}

	@GetMapping("/get/{name}") // http://localhost:8080/get/priya
	ResponseEntity<?> findByName(@PathVariable String name) {
		List<Employee> findByName = service.findByName(name);
		return new ResponseEntity<List<Employee>>(findByName, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	ResponseEntity<?> delete(@PathVariable Integer id) {
		System.out.println("Deleted");
		Employee deleteEmployee = service.deleteEmployee(id);
		return new ResponseEntity<String>("Deleted", HttpStatus.OK);
	}

}
